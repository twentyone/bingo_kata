<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Model\Bingo;
use App\Model\Player;

class BingoTest extends TestCase
{

    public function test_start_bingo_game()
    {
        $bingo = new Bingo();
        $bingo->startGame(5);

        $this->assertInstanceOf(Player::class, $bingo->players[0]);
    }

    public function test_player_calls_Bingo_after_all_numbers_on_their_card_have_been_called(
    )
    {
        $bingo = new Bingo();
        $bingo->startGame(5);

        $bingo->bingoCaller->shoutedNumbers = [
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
            20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
            37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53,
            54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
            71, 72, 73, 74, 75,
        ];
        $bingo->players[3]->card->numbers   = [
            5, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
            37, 38, 39, 40, 41, 62, 63, 64, 65, 66, 67, 1, 2, 3,
        ];
        $bingo->players[3]->crossedNumbers  = [
            5, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
            37, 38, 39, 40, 41, 62, 63, 64, 65, 66, 67, 1, 2, 3,
        ];

        $this->assertTrue($bingo->players[3]->callBingo());
    }

    public function test_player_calls_bingo_before_all_numbers_on_their_card_have_been_called(
    )
    {
        $bingo = new Bingo();
        $bingo->startGame(5);
        $bingo->nextNumber();
        $bingo->nextNumber();
        $bingo->nextNumber();

        $this->assertFalse($bingo->players[2]->callBingo());
    }
}
