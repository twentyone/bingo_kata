<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Model\UsCardGenerator;

class UsCardGeneratorTest extends TestCase
{

    /**
     *
     * @return void
     */
    public function test_generate_us_bingo_card()
    {
        $generator = new UsCardGenerator;
        $card      = $generator->generate();

        $this->assertCount(25, $card);
    }

    public function test_unique_card_values()
    {
        $generator = new UsCardGenerator;
        $card      = $generator->generate();
        $card2     = array_unique($card);
        $this->assertCount(25, $card2);
    }

}
