<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Player;

class PlayerGenerator extends Model
{

    public function generate($numberPlayers)
    {
        $players = [];

        for ($i = 0; $i < $numberPlayers; $i++) {
            $players[$i] = new Player();
        }

        return $players;
    }
}
