<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UsCardGenerator extends Model
{

    public function generate()
    {
        $bingo_card = [];
        $columns    = [
            range(1, 15),
            range(16, 30),
            range(31, 45),
            range(46, 60),
            range(61, 75),
        ];

        for ($j = 0; $j < 5; $j++) {
            $random_keys   = array_rand($columns[$j], 5);
            $random_values = array_intersect_key($columns[$j],
                array_flip($random_keys));
            $bingo_card    = array_merge($bingo_card, $random_values);
        }

        // Free space
        $bingo_card[13] = '';

        return $bingo_card;
    }
}
