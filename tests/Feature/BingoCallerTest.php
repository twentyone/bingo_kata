<?php

namespace Tests\Feature;

use App\Model\BingoCaller;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BingoCallerTest extends TestCase
{

    public function test_number_shouted_is_between_1_and_75()
    {
        $bingoCaller = new BingoCaller();
        $number      = $bingoCaller->shoutNumber();

        $this->assertRegExp('/^[1-9][0-9]?$|^75$/', $number);
    }

    public function test_bingocaller_calls_75_times()
    {
        // Bingo number from 1 to 75
        $bingoNumbers = [
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
            20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
            37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53,
            54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
            71, 72, 73, 74, 75
        ];

        $bingoCaller = new BingoCaller();

        for ($i = 1; $i <= 75; $i++) {
            $bingoCaller->shoutNumber();
        }

        $shoutedNumbers = $bingoCaller->shoutedNumbers;
        sort($shoutedNumbers);

        $this->assertEquals($bingoNumbers, $shoutedNumbers);
    }
}
