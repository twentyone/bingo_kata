<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{

    public $card;
    public $crossedNumbers;

    public function __construct()
    {
        $this->card           = new Card();
        $this->crossedNumbers = [];
    }

    public function checkNumberInCard(int $number)
    {
        if ($this->card->contains($number)) {
            $this->crossedNumbers[] = $number;

            return true;
        }

        return false;
    }

    public function callBingo()
    {
        return $this->checkAllCrossed();
    }

    public function checkAllCrossed()
    {
        return count(array_diff($this->card->numbers,
                $this->crossedNumbers)) === 0;
    }
}
