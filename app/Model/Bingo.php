<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\PlayerGenerator;

class Bingo extends Model
{

    public $players;
    public $bingoCaller;
    public $winner;

    public function __construct()
    {
        $this->bingoCaller = new BingoCaller();
        $this->winner      = new Player();
    }

    public function startGame($numberPlayers)
    {
        $playerGenerator = new PlayerGenerator();
        $this->players   = $playerGenerator->generate($numberPlayers);
    }

    public function finishGame()
    {
        $this->continueBingo();
    }

    public function nextNumber()
    {
        $currentNumber = $this->bingoCaller->shoutNumber();
        foreach ($this->players as $player) {
            $player->checkNumberInCard($currentNumber);
            if ($player->checkAllCrossed() && $this->bingoCaller->checkWinnerNumbers($player->card->numbers)) {
                $this->winner = $player;
            }
        }
    }

    public function continueBingo()
    {
        // While the BingoCaller doesn't finish the game
        while ( ! $this->bingoCaller->endGame()) {

            $currentNumber = $this->bingoCaller->shoutNumber();
            foreach ($this->players as $player) {
                $player->checkNumberInCard($currentNumber);
                if ($player->checkAllCrossed() && $this->bingoCaller->checkWinnerNumbers($player->card->numbers)) {
                    $this->winner = $player;
                }
            }
        }
    }

    public function executeEntireGame()
    {
        $this->startGame();
        $this->finishGame();
    }
}
