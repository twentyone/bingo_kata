<?php

namespace Tests\Feature;

use App\Model\Player;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PlayerTest extends TestCase
{

    public function test_all_numbers_crossed_in_card()
    {
        $player                 = new Player();
        $player->card->numbers  = [
            5, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
            37, 38, 39, 40, 41, 62, 63, 64, 65, 66, 67, 1, 2, 3,
        ];
        $player->crossedNumbers = [
            5, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
            37, 38, 39, 40, 41, 62, 63, 64, 65, 66, 67, 1, 2, 3,
        ];

        $this->assertTrue($player->checkAllCrossed());
    }

    public function test_check_number_in_card()
    {
        $player                = new Player();
        $player->card->numbers = [
            5, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
            37, 38, 39, 40, 41, 62, 63, 64, 65, 66, 67, 1, 2, 3,
        ];

        $this->assertTrue($player->checkNumberInCard(3));
    }
}
