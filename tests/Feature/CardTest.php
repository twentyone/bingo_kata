<?php

namespace Tests\Feature;

use App\Model\Card;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CardTest extends TestCase
{

    public function test_card_only_only_has_25_unique_spaces()
    {
        $card = new Card();
        $this->assertCount(25, $card->numbers);
    }

    /**
     * @test
     * @dataProvider ranges
     *
     * @param $ranges
     * @param $limits
     */
    public function test_card_columns_only_contains_numbers_between_lower_and_upper_bound_inclusive(
        $ranges,
        $limits
    ) {
        $isValid = false;
        $card    = new Card();

        for ($i = $ranges[0]; $i < $ranges[1]; $i++) {
            if ($card->numbers[$i] >= $limits[0] && $card->numbers[$i] <= $limits[1]) {
                $isValid = true;
            }
        }

        $this->assertTrue($isValid);
    }

    public function test_card_generated_has_free_space_in_the_middle()
    {
        $card = new Card();
        $this->assertEquals('', $card->numbers[13]);
    }

    public function ranges()
    {
        // Range and Limits
        return [
            [[0, 5], [1, 15]],
            [[6, 10], [16, 30]],
            [[11, 15], [31, 45]],
            [[16, 20], [46, 60]],
            [[20, 25], [61, 75]],
        ];
    }
}
