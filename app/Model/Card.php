<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{

    public $numbers;

    public function __construct()
    {
        $uscardGenerator = new UsCardGenerator();
        $this->numbers   = $uscardGenerator->generate();
    }

    public function contains(int $number)
    {
        return in_array($number, $this->numbers);
    }
}
