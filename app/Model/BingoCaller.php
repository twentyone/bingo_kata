<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BingoCaller extends Model
{

    public $numbersToShout = [];
    public $shoutedNumbers;
    public $totalShouted;
    public $existsWinner;

    public function __construct()
    {
        $this->numbersToShout = $this->generateNumbers();
        $this->shoutedNumbers = [];
        $this->totalShouted   = 0;
        $this->existsWinner   = false;
    }

    public function shoutNumber()
    {
        $number                 = $this->numbersToShout[$this->totalShouted];
        $this->shoutedNumbers[] = $number;
        $this->totalShouted++;

        return $number;
    }

    public function generateNumbers()
    {
        $bingoNumbers = range(1, 75);
        shuffle($bingoNumbers);

        return $bingoNumbers;
    }

    public function checkWinnerNumbers(array $numbers)
    {
        $this->existsWinner = count(array_intersect($this->shoutedNumbers,
                $numbers)) === count($numbers);

        return $this->existsWinner;
    }

    public function endGame()
    {
        return $this->totalShouted === count($this->numbersToShout) || $this->existsWinner;
    }
}
